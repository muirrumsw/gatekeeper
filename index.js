const Discord = require('discord.js');
const client = new Discord.Client();
const config = require('./config.json');
const Keyv = require('keyv');

const mods = new Keyv('sqlite://db/mods.sqlite');
const bans = new Keyv('sqlite://db/bans.sqlite');

client.on('ready', () => {
	console.log("Logged in.");
});

client.on('guildMemberAdd', async member => {
	console.log(`New member ${member.user.username}#${member.user.discriminator} joined ${member.guild.name}.\nChecking ban status...`)
	var isBanned = await bans.get(member.id);
	console.log(`User ${isBanned == 1 ? "is" : "is not"} banned.`);
	if (isBanned == 1) {
		if (member.kickable) {
			member.kick(`Found in Gatekeeper blacklist`);
		} else {
			member.guild.systemChannel.send(`Attention! User <@${member.user.id}> has joined the server and has been found on the ban list. Gatekeeper cannot kick/ban this user.`);
		}
	}
})

client.on('message', async message => {
	if (message.content.startsWith(config.prefix)) {
		console.log('Found a command');
		if (message.content.startsWith(config.prefix + "addBan")) {
			message.reply('Started addBan');
			var args = message.content.split(' ')[1];
			message.reply(`Checking mod status`);
			var isMod = await mods.get(message.author.id);
			message.reply(`User is ${isMod == 1 ? "a" : "not a"} mod`)
			if (isMod == 1) {
				await bans.set(args, 1);
				message.reply(`Banned ${args}`);
			}
		} else if (message.content.startsWith(config.prefix + "addmod")) {
			message.reply(`Started addMod`);
			var newMod = message.mentions.members.first();
			if (message.author.id == config.owner && newMod != null) {
				mods.set(newMod.id, 1);
				message.reply(`Added a mod`);
			}
		} else if (message.content.startsWith(config.prefix + "kickban")) {
			var target = message.mentions.members.first();
			console.log(`Started kickBan`)
			var isMod = await mods.get(message.author.id);
			console.log(`User ${isMod == 1 ? "is a" : "is not a"} mod`);
			if (isMod == 1) {
				await bans.set(target.id, 1);
				console.log(`Set mode +b on user ${target.user.username}#${target.user.discriminator}`);
				if (target.kickable) {
					target.kick(`Requested (${message.author.username}#${message.author.discriminator})`);
					message.reply(`Set mode +b on ${target.user.username}#${target.user.discriminator}\nKicked user from server (Requested (${message.author.username}#${message.author.discriminator}))`);
				}
			}
		} else if (message.content.startsWith(config.prefix + "help")) {
			channel = message.channel;
			channel.send(`**addmod @NewMod** - Adds a mod to the database. You must be the bot owner to do this.\n\n**addBan UserID** - Adds a new ban to the database. This does not kick the user, it only prevents them from rejoining.\n\n**kickban @User** - Adds a new ban to the database *and* kicks the user from the server.`);
		}

	}
});

client.login(config.token);
